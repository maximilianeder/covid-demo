package com.maximilianeder.camundademo;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

public class SpeichereErhalt implements JavaDelegate {
    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        String employee = (String) delegateExecution.getVariable("receiver");
        String richtlinie = (String) delegateExecution.getVariable("richtlinie");

        DatabaseConnector.logAcknowledgingEmployees(richtlinie, employee);
    }
}
