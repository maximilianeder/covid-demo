package com.maximilianeder.camundademo;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.spin.impl.json.jackson.JacksonJsonNode;

public class TransformVariables implements JavaDelegate {
    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        String receiver = ((JacksonJsonNode) delegateExecution.getVariable("receiver")).stringValue();
        delegateExecution.setVariable("receiver", receiver);
    }
}
