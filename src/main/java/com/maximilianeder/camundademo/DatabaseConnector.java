package com.maximilianeder.camundademo;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DatabaseConnector {

    private static final String DB_URL = "jdbc:sqlite:src/db/covid-demo.db";

    public static List<Map<String,String>> getEmployeesAndEmails (){
        List<Map<String,String>> list = new ArrayList<>();

        String sqlQuery = "SELECT name, email FROM employees";

        try {
            Connection con = DriverManager.getConnection(DB_URL);
            Statement stat = con.createStatement();
            ResultSet rs = stat.executeQuery(sqlQuery);

            while(rs.next()){
                String name = rs.getString("name");
                String email = rs.getString("email");
                Map<String, String> map = Map.of("label", name, "value", email);
                list.add(map);
            }

            con.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return list;
    }

    public static void logAcknowledgingEmployees(String guideline, String employee){
        String sqlQuery =
                "INSERT INTO acknowledged (guideline, employee) VALUES ('" + guideline + "', '" + employee + "')";

        try {
            Connection con = DriverManager.getConnection(DB_URL);
            Statement stat = con.createStatement();
            stat.execute(sqlQuery);

            con.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }



    public static Tuple<String,String> getEmployeeNameAndSuperior (String employeeMail){
        String sqlQuery =
                "SELECT a.name as name, b.email as superior " +
                "FROM employees a LEFT OUTER JOIN employees b ON a.superior = b.ID " +
                "WHERE a.email = '" + employeeMail + "'";

        try {
            Connection con = DriverManager.getConnection(DB_URL);
            Statement stat = con.createStatement();
            ResultSet rs = stat.executeQuery(sqlQuery);

            String name = null, superior = null;

            while(rs.next()){
                name = rs.getString("name");
                superior = rs.getString("superior");
            }

            con.close();
            return new Tuple<>(name, superior);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}

