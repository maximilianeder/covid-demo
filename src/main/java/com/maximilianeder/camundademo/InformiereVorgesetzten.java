package com.maximilianeder.camundademo;

import com.fasterxml.uuid.Logger;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class InformiereVorgesetzten implements JavaDelegate {
    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        String employee  = (String) delegateExecution.getVariable("receiver");

        Tuple<String, String> dataTuple = DatabaseConnector.getEmployeeNameAndSuperior(employee);
        String employeeName = dataTuple.x;
        String superiorMail = dataTuple.y;

        if (superiorMail == null)
            return;

        String requestString = generateRequestString(employeeName, superiorMail);

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://localhost:8080/engine-rest/message"))
                .header("Content-Type", "application/json")
                .method("POST", HttpRequest.BodyPublishers.ofString(requestString))
                .build();

        HttpResponse<String> response = null;

        try {
            response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());

            if (response != null) {
                System.out.println(response.statusCode());
                System.out.println(response.body());
            }else{
                System.out.println("Response from InformiereVorgesetzten returned null");
            }
        }catch (IOException e){
            //e.printStackTrace();
            Logger.logError(e.getMessage());
        }
    }

    private String generateRequestString(String employeeName, String superiorMail){
        return "{\n" +
                "    \"messageName\": \"reminderSuperior\",\n" +
                "    \"processVariables\": {\n" +
                "        \"employeeName\": {\n" +
                "            \"value\": \"" + employeeName + "\",\n" +
                "            \"type\": \"String\"\n" +
                "        }, \n" +
                "        \"superior\": {\n" +
                "            \"value\": \"" + superiorMail + "\",\n" +
                "            \"type\": \"String\"\n" +
                "        }\n" +
                "    },\n"+
                "    \"resultEnabled\": true\n" +
                "}";
    }
}
