package com.maximilianeder.camundademo;

import com.fasterxml.uuid.Logger;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class InformierePersonalabteilung implements JavaDelegate {
    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        String employee  = (String) delegateExecution.getVariable("receiver");

        Tuple<String, String> dataTuple = DatabaseConnector.getEmployeeNameAndSuperior(employee);
        String employeeName = dataTuple.x;

        String requestString = generateRequestString(employeeName);

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://localhost:8080/engine-rest/message"))
                .header("Content-Type", "application/json")
                .method("POST", HttpRequest.BodyPublishers.ofString(requestString))
                .build();

        HttpResponse<String> response = null;

        try {
            response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        }catch (IOException e){
            Logger.logError(e.getMessage());
        }
        if (response != null) {
            System.out.println(response.statusCode());
            System.out.println(response.body());
        }
    }

    private String generateRequestString(String employeeName){
        return "{\n" +
                "    \"messageName\": \"reminderHR\", \n" +
                "    \"processVariables\": {\n" +
                "        \"employeeName\": {\n" +
                "            \"value\": \"" + employeeName + "\",\n" +
                "            \"type\": \"String\"\n" +
                "        } \n" +
                "   } \n" +
                "}";
    }
}
