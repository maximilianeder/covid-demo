package com.maximilianeder.camundademo;

import com.fasterxml.uuid.Logger;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class SendeRichtlinie implements JavaDelegate {
    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        String receiver  = (String) delegateExecution.getVariable("receiver");
        String richtlinie = (String) delegateExecution.getVariable("richtlinie");

        String requestString = generateRequestString(receiver, richtlinie);

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://localhost:8080/engine-rest/message"))
                .header("Content-Type", "application/json")
                .method("POST", HttpRequest.BodyPublishers.ofString(requestString))
                .build();

        HttpResponse<String> response = null;

        try {
            response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        }catch (IOException e){
            //e.printStackTrace();
            Logger.logError(e.getMessage());
        }
        if (response != null) {
            System.out.println(response.statusCode());
            System.out.println(response.body());
        }
    }

    private String generateRequestString(String receiver, String richtlinie){
        return "{\n" +
                "    \"messageName\": \"NeueRichtlinie\",\n" +
                "    \"processVariables\": {\n" +
                "        \"richtlinie\": {\n" +
                "            \"value\": \"" + richtlinie + "\",\n" +
                "            \"type\": \"String\"\n" +
                "        }, \n" +
                "        \"employee\": {\n" +
                "            \"value\": \"" + receiver + "\",\n" +
                "            \"type\": \"String\"\n" +
                "        }\n" +
                "    },\n" +
                "    \"resultEnabled\": true\n" +
                "}";
    }

    @Deprecated
    private void legacyExecution(String receiver, String richtlinie){
        String email_body = "Sending mail to " + receiver + ": \n" +
                "Please download our new regulation from " + richtlinie;

        System.out.println(email_body);
    }
}
