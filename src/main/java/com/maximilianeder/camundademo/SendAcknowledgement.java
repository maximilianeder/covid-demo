package com.maximilianeder.camundademo;

import com.fasterxml.uuid.Logger;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class SendAcknowledgement implements JavaDelegate {
    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        String employee  = (String) delegateExecution.getVariable("employee");

        String requestString = generateRequestString(employee);

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://localhost:8080/engine-rest/message"))
                .header("Content-Type", "application/json")
                .method("POST", HttpRequest.BodyPublishers.ofString(requestString))
                .build();

        HttpResponse<String> response = null;

        try {
            response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        }catch (IOException e){
            //e.printStackTrace();
            Logger.logError(e.getMessage());
        }
        if (response != null) {
            System.out.println(response.statusCode());
            System.out.println(response.body());
        }
    }

    private String generateRequestString(String employee){
        return "{\n" +
                "    \"messageName\": \"Acknowledge_" + employee + "\"\n" +
                "}";
    }
}
