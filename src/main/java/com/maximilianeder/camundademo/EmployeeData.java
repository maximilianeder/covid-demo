package com.maximilianeder.camundademo;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.camunda.spin.plugin.variable.SpinValues;
import org.camunda.spin.plugin.variable.value.JsonValue;
import org.json.JSONArray;

import javax.xml.crypto.Data;
import java.sql.*;

public class EmployeeData implements JavaDelegate {

    @Override
    public void execute(DelegateExecution delegateExecution) {
        List<Map<String, String>> employees_map = DatabaseConnector.getEmployeesAndEmails();
        String employees_s = new JSONArray(employees_map).toString();
        JsonValue employees = SpinValues.jsonValue(employees_s).create();

        delegateExecution.setVariable("employees", employees);
    }

    public static void main(String[] args){
        List<Map<String, String>> list = DatabaseConnector.getEmployeesAndEmails();
        System.out.println(list.get(0));
        System.out.println(list.get(1));
        System.out.println(list.get(5));
    }
}